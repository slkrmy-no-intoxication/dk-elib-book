# Note that Angular does not support HTML Entity Code and can only support Hex Code. 

# Some characters which have no HTML Entity Code are new set of non-English 
  characters which cannot be displayed or not supported by some old systems. 
  
# In order to support old system, the fall back is to just use characters in 
  ASCII set (a to z) to represent some Pali characters which do not have 
  HTML Entity Codes; for example, since n with a dot below has no HTML Entity 
  Code, use normal n to replace it. However since technology is moving fast 
  and the old systems will become obsolete, we will not adopt the fall-back 
  but to use hex code for Pali character even if it does not have HTML Entity
  Code. 
  
# For other characters code not listed below, please refer to the following web site. 
  ~ https://www.w3schools.com/charsets/ref_utf_latin_extended_a.asp
  ~ https://www.toptal.com/designers/htmlarrows/letters/ 

------------------------------------
# HTML Entity Code "&amacr;" and HEX Hexidecimal Code ("&#x101;") for frequently used Pali letters/characters: 

# Big letter A with macron (bar) above it.
  HTML Entity: &Amacr; 
  HEX Code:    &#x100;   

# Small letter a with macron (bar) above it. 
  HTML Entity: &amacr; 
  HEX Code:    &#x101;  

# Small letter d with dot below it.
  HEX Code:	&#x1e0d;
  
# Big letter I with macron (bar) above it. 
  HTML Entity: &Imacr; 
  HEX Code: &#x12a; 

# Small letter i with macron (bar) above it. 
  HTML Entity: &imacr;
  HEX Code: &#x12b; 

# Big letter M with dot below it. 
  HTML Entity: Not available 
  HEX Code: &#x1e42; 

# Small letter m with dot below it.
  HTML Entity: Not available 
  HEX Code: &#x1e43; 

# Small letter m with dot above it.
  HTML Entity: Not available 
  HEX Code: &#x1E43; 
  
# Big letter N with dot below it. 
  HTML Entity: Not available 
  HEX Code: &#x1e46; 

# Small letter n with dot below it. 
  HTML Entity: Not available 
  HEX Code: &#x1e47; 

# Big letter N with tilde above it. 
  HTML Entity: &Ntilde;
  HEX Code: &#xd1; 

# Small letter n with tilde above it. 
  HTML Entity: &ntilde; 
  HEX Code: &#x00f1; 

# Big letter N with dot above it. 
  HTML Entity: Not available 
  HEX Code: &#x1e44;   

# Small letter n with dot above it. 
  HTML Entity: Not available 
  HEX Code: &#x1e45; 

# Big letter O with macron (bar) above it.
  HTML Entity: &Omacr; 
  HEX Code:    &#x014C;   

# Small letter o with macron (bar) above it. 
  HTML Entity: &omacr; 
  HEX Code:    &#x014D;  

# Big letter O with tilde above it. 
  HTML Entity: &Otilde; 
  HEX Code: &#xd5; 

# Small letter o with tilde above it. 
  HTML Entity: &otilde;  
  HEX Code: &#xf5; 

# Big letter T with dot below it. 
  HTML Entity: Not available 
  HEX Code: &#x1e6c;  

# Small letter t with dot below it. 
  HTML Entity: Not available 
  HEX Code: &#x1e6d; 

# Big letter U with macron (bar) above it. 
  HTML Entity: &Umacr; 
  HEX Code: &#x16a;  

# Small letter u with macron (bar) above it. 
  HTML Entity: &umacr; 
  HEX Code: &#x16b;  

# Big letter U with tilde above it. 
  HTML Entity: &Utilde; 
  HEX Code: &#x0168;  

# Small letter u with tilde above it. 
  HTML Entity: &utilde; 
  HEX Code: &#x0169;  

# Big letter U with ring above it. 
  HTML Entity: &Uring; 
  HEX Code: &#x016E;  

# Small letter u with ring above it. 
  HTML Entity: &uring; 
  HEX Code: &#x016F;  

# Copyright Sign 
  HTML Entity: &copy;
  HEX Code: &#xa9;  

# Registered Sign 
  HTML Entity: &reg;
  HEX Code: &#xae; 

# Beamed Eighth Notes (Music Symbol) 
  HEX Code: &#x266B; 

# Black Star
  HEX Code: &#x2605; 

# White Star  
  HEX Code: &#x2606; 

# White Diamond Suit
  HEX Code: &#x2662; 

# Black Diamond Suit 
  HTML Entity: &diams; 
  HEX Code: &#x2666; 

# White Heart Suit 
 HEX Code: &#x2661; 

# Black Heart Suit 
  HTML Entity: &hearts; 
  HEX Code: &#x2665; 

# Black Spade Suit 
  HTML Entity: &spades; 
  HEX Code: &#x2660;  

# White Spade Suit
  HEX Code: &#x2664;  

# Black Club Suit 
  HTML Entity: &clubs; 
  HEX Code: &#x2663; 

# White Club Suit 
  HEX Code: &#x2667; 

------------------------------------
